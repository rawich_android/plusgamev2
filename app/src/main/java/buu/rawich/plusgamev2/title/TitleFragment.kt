package buu.rawich.plusgamev2.title

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.rawich.plusgamev2.R
import buu.rawich.plusgamev2.databinding.FragmentPlayGameBinding
import buu.rawich.plusgamev2.databinding.FragmentTitleBinding
import buu.rawich.plusgamev2.play.PlayGameViewModel



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [TitleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TitleFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentPlayGameBinding
    private lateinit var viewModel: PlayGameViewModel
    private var menu = 0
    private var pointCorrect = 0
    private var pointIncorrect = 0


    override fun onCreateView(   inflater: LayoutInflater, container: ViewGroup?,
                                 savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentTitleBinding>(inflater,
            R.layout.fragment_title, container, false)

        pointCorrect = TitleFragmentArgs.fromBundle(requireArguments()).pointCorrect
        pointIncorrect = TitleFragmentArgs.fromBundle(requireArguments()).pointIncorrect

        binding.apply {
            playPlusGamebtn.setOnClickListener {
                menu = 1
                view?.findNavController()?.navigate(
                    TitleFragmentDirections.actionTitleFragmentToPlusGameFragment(
                        pointCorrect,
                        pointIncorrect,
                        menu
                    )
                )
            }
            playMinusGamebtn.setOnClickListener {
                menu = 2
                view?.findNavController()?.navigate(
                    TitleFragmentDirections.actionTitleFragmentToPlusGameFragment(
                        pointCorrect,
                        pointIncorrect,
                        menu
                    )
                )
            }
            playMultiGamebtn.setOnClickListener {
                menu = 3
                view?.findNavController()?.navigate(
                    TitleFragmentDirections.actionTitleFragmentToPlusGameFragment(
                        pointCorrect,
                        pointIncorrect,
                        menu
                    )
                )
            }
            playDivideGamebtn.setOnClickListener {
                menu = 4
                view?.findNavController()?.navigate(
                    TitleFragmentDirections.actionTitleFragmentToPlusGameFragment(
                        pointCorrect,
                        pointIncorrect,
                        menu
                    )
                )
            }
            resetbtn.setOnClickListener {
                pointCorrect = 0
                pointIncorrect = 0
                binding.txtMenuPointCorrect.text = pointCorrect.toString()
                binding.txtMenuPointIncorrect.text = pointIncorrect.toString()
            }
            binding.txtMenuPointCorrect.text = pointCorrect.toString()
            binding.txtMenuPointIncorrect.text = pointIncorrect.toString()
        }
        println("menu "+menu)
        return binding.root
    }

}