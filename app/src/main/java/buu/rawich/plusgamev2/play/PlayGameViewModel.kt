package buu.rawich.plusgamev2.play

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class PlayGameViewModel(pointCorrect: Int, pointIncorrect: Int, menu: Int) : ViewModel() {

    private val _num1 = MutableLiveData<Int>()
    val num1: LiveData<Int>
        get() = _num1

    private val _num2 = MutableLiveData<Int>()
    val num2: LiveData<Int>
        get() = _num2

    private val _txtSign = MutableLiveData<String>()
    val txtSign: LiveData<String>
        get() = _txtSign

    private val _btn1 = MutableLiveData<Int>()
    val btn1: LiveData<Int>
        get() = _btn1

    private val _btn2 = MutableLiveData<Int>()
    val btn2: LiveData<Int>
        get() = _btn2

    private val _btn3 = MutableLiveData<Int>()
    val btn3: LiveData<Int>
        get() = _btn3

    private val _pointIncorrect = MutableLiveData<Int>()
    val pointIncorrect: LiveData<Int>
        get() = _pointIncorrect

    private val _pointCorrect = MutableLiveData<Int>()
    val pointCorrect: LiveData<Int>
        get() = _pointCorrect

    private val _txtpointCorrect = MutableLiveData<String>()
    val txtpointCorrect: LiveData<String>
        get() = _txtpointCorrect

    private val _txtpointIncorrect = MutableLiveData<String>()
    val txtpointIncorrect: LiveData<String>
        get() = _txtpointIncorrect
    init {
        _num1.value = 0
        _num2.value = 0
        _txtSign.value = ""
        _pointCorrect.value = 1
        _pointIncorrect.value = menu
        _txtpointCorrect.value = ""
        _txtpointIncorrect.value = ""
        // setQuestion(menu)
        play(menu)
        Log.i("CalculateViewModel", "CalculateViewModel created!")

    }
    private fun play(menu: Int) {
        val result = setQuestion(menu)
        randomButton(result)
        // checkClick(result)
        //checkClick(result)
        // checkClick(result)
    }
    private fun randomButton(result: Int) {
        val randomNum = Random.nextInt(1, 4)
        if (randomNum == 1) {
            _btn1.value = result
            _btn2.value = result?.plus(1)
            _btn3.value = result?.minus(1)
        } else if (randomNum == 2) {
            _btn1.value = result?.plus(1)
            _btn2.value = result
            _btn3.value = result?.minus(1)
        } else {
            _btn1.value = result?.plus(1)
            _btn2.value = result?.minus(1)
            _btn3.value = result

        }
    }
    private fun setQuestion(menu: Int): Int {
        var result = 0
        var number1 = Random.nextInt(0, 10)
        var number2 = Random.nextInt(0, 10)

        _num1.value = number1
        _num2.value = number2

        if (menu == 1){
            result = number1 + number2
            _txtSign.value = "+"

        }else if(menu == 2){
            result = number1 - number2
            _txtSign.value = "-"

        }else if (menu == 3) {
            result = number1 * number2
            _txtSign.value = "*"

        }else {
            while (true){
                number2 = Random.nextInt(1,10)
                if (number1 % number2 ==0){
                    result = number1 / number2
                    _txtSign.value = "/"
                    break
                }
            }
        }

        return result
    }

}
